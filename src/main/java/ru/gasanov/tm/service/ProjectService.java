package ru.gasanov.tm.service;

import ru.gasanov.tm.entity.Task;
import ru.gasanov.tm.entity.User;
import ru.gasanov.tm.entity.UserRole;
import ru.gasanov.tm.repository.ProjectRepository;
import ru.gasanov.tm.entity.Project;
import ru.gasanov.tm.repository.UserRepository;

import java.util.Collections;
import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;
    private final AuthService authService;


    public ProjectService(ProjectRepository repository, UserRepository userRepository, AuthService authService) {
        this.projectRepository = repository;
        this.userRepository = userRepository;
        this.authService = authService;
    }


    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public boolean assignProjectToUser(Long userId, Long taskId){
        if (authService.getCurrentUser() == null){
            return false;
        }
        Project project = projectRepository.findById(taskId);
        User user = userRepository.findById(userId);
        if (project == null || user == null){
            return false;
        }
        project.setUserId(userId);
        projectRepository.save(project);
        return true;
    }

    public boolean detachProjectToUser(Long userId, Long taskId){
        if (authService.getCurrentUser() == null){
            return false;
        }
        Project project = projectRepository.findById(taskId);
        User user = userRepository.findById(userId);
        if (project == null || user == null){
            return false;
        }
        project.setUserId(null);
        projectRepository.save(project);
        return true;
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project create(String name, String description) {
        if (description == null || description.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project update(Long id, String name, String description) {
        if (description == null || description.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        if (id == null ) return null;
        return projectRepository.update(id, name, description);
    }

    public Project findByIndex(int index) {
        if (index < 0 || index > projectRepository.size() - 1) return null;
        return projectRepository.findByIndex(index);
    }

    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    public Project removeById(Long id) {
        if (id == 0) return null;
        return projectRepository.removeById(id);
    }

    public Project removeByIndex(int index) {
        if (index < 0 || index > projectRepository.size() - 1) return null;
        return projectRepository.removeByIndex(index);
    }

    public Project findById(Long id) {
        if (id == 0) return null;
        return projectRepository.findById(id);
    }

    public static void main(String[] args) {
        ProjectRepository.main(args);
    }

    public List<Project> getAvaliableTasks() {
        User currentUser = authService.getCurrentUser();
        if (currentUser == null){
            return Collections.emptyList();
        }

        if (currentUser.getRole() == UserRole.ADMIN){
            return projectRepository.findAll();
        } else {
            return projectRepository.findByUserId(currentUser.getId());
        }
    }
}

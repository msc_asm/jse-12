package ru.gasanov.tm.service;

import ru.gasanov.tm.entity.User;
import ru.gasanov.tm.entity.UserRole;
import ru.gasanov.tm.repository.TaskRepository;
import ru.gasanov.tm.entity.Task;
import ru.gasanov.tm.repository.UserRepository;

import java.util.Collections;
import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final AuthService authService;

    public TaskService(TaskRepository taskRepository, UserRepository userRepository, AuthService authService) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.authService = authService;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task create(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    public Task update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public boolean assignTaskToUser(Long userId, Long taskId){
        if (authService.getCurrentUser() == null){
            return false;
        }
        Task task = taskRepository.findById(taskId);
        User user = userRepository.findById(userId);
        if (task == null || user == null){
            return false;
        }
        task.setUserId(userId);
        taskRepository.save(task);
        return true;
    }

    public List<Task> getAvaliableTasks(){
        User currentUser = authService.getCurrentUser();
        if (currentUser == null){
            return Collections.emptyList();
        }

        if (currentUser.getRole() == UserRole.ADMIN){
            return taskRepository.findAll();
        } else {
            return taskRepository.findByUserId(currentUser.getId());
        }

    }

    public boolean detachTaskToUser(Long userId, Long taskId){
        if (authService.getCurrentUser() == null){
            return false;
        }
        Task task = taskRepository.findById(taskId);
        User user = userRepository.findById(userId);
        if (task == null || user == null){
            return false;
        }
        task.setUserId(null);
        taskRepository.save(task);
        return true;
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public Task findById(Long id) {
        if (id == null) return null;
        return taskRepository.findById(id);
    }

    public Task findByIndex(int index) {
        if (index < 0 || index > taskRepository.size() - 1) return null;
        return taskRepository.findByIndex(index);
    }

    public Task removeById(Long id) {
        if (id == null ) return null;
        return taskRepository.removeById(id);
    }

    public Task removeByIndex(int index) {
        if (index < 0 || index > taskRepository.size() -1) return null;
        return taskRepository.removeByIndex(index);
    }

    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    public List<Task> findAllByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task findByProjectIdAndId(Long projectId, Long id) {
        if (projectId == null || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

}

